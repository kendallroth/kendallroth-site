import React from 'react';
import PropTypes from 'prop-types';

// Styles
import { IconImageSC, IconLinkSC, IconBarSC } from './styled';

/**
 * Icon bar
 * @param {string}   color - Background icon colour
 * @param {object[]} icons - List of icons and properties
 * @param {boolean}  round - Whether icons are round
 * @param {number}   size  - Icon size
 */
const IconBar = (props) => {
  const { color, icons, round, size } = props;

  if (!Boolean(icons) || icons.length === 0) return null;

  return (
    <IconBarSC>
      {icons.map((i) => (
        <IconLinkSC
          key={i.url}
          alt={i.name}
          color={color}
          href={i.url}
          round={round}
          target="__blank"
        >
          <IconImageSC size={size} src={i.img} />
        </IconLinkSC>
      ))}
    </IconBarSC>
  );
};

IconBar.propTypes = {
  color: PropTypes.string,
  icons: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      img: PropTypes.any.isRequired,
      url: PropTypes.string.isRequired,
    })
  ),
  round: PropTypes.bool,
  size: PropTypes.number,
};

IconBar.defaultProps = {
  color: 'transparent',
  icons: [],
  round: true,
  size: 32,
};

export default IconBar;
