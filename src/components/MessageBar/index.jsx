import React from 'react';
import PropTypes from 'prop-types';

// Styles
import { MessageBarSC } from './styled';

/**
 * Display a fullwidth message bar
 * @param {string}  background - Background colour
 * @param {string}  borderTop  - Top border color
 * @param {element} children   - Message bar content
 * @param {string}  color      - Text colour
 */
const MessageBar = (props) => {
  const { background, borderTop, children, color } = props;

  return (
    <MessageBarSC background={background} borderTop={borderTop} color={color}>
      {children}
    </MessageBarSC>
  );
};

MessageBar.propTypes = {
  background: PropTypes.string,
  borderTop: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string])
    .isRequired,
  color: PropTypes.string,
};

MessageBar.defaultProps = {
  background: 'black',
  borderTop: null,
  color: 'white',
};

export default MessageBar;
