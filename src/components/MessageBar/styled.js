import styled from 'styled-components';

/**
 * Message bar component
 * @param {string} background - Background colour
 * @param {string} borderTop  - Border color (optional)
 * @param {string} color      - Text colour
 */
const MessageBarSC = styled.div`
  padding: 1.5em;
  text-align: center;
  font-weight: bold;
  color: ${({ color }) => color};
  background-color: ${({ background }) => background};

  ${({ borderTop }) => borderTop && `border-top: 4px solid ${borderTop}`}
`;

export { MessageBarSC };

/* eslint import/prefer-default-export: off */
