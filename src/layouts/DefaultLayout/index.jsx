import React from 'react';
import PropTypes from 'prop-types';
import { graphql, StaticQuery } from 'gatsby';
import Helmet from 'react-helmet';

// Components
import Header from '@components/Header';

// Images
import favicon from '@assets/icons/favicon.png';

// Styles
import '@assets/foundation.css';
import './index.css';
import { GatsbyBodySC } from './styled';

/**
 * Default layout wrapper
 * @param {object} children - React children
 */
const DefaultLayout = ({ children }) => {
  const description =
    'Portfolio site for Kendall Roth, web developer and other stuff';
  const keywords = 'portfolio, gatsby, kendall roth, web developer';

  return (
    <StaticQuery
      query={query}
      render={(data) => {
        const { title } = data.site.siteMetadata;

        return (
          <GatsbyBodySC>
            <Helmet
              defaultTitle={title}
              titleTemplate={`${title} - %s`}
              meta={[
                { name: 'description', content: description },
                { name: 'og:description', content: description },
                { name: 'og:title', content: title },
                { name: 'og:type', content: 'website' },
                { name: 'keywords', content: keywords },
              ]}
            >
              <html lang="en" />
              <link
                href="https://fonts.googleapis.com/icon?family=Material+Icons"
                rel="stylesheet"
              />
              <link rel="shortcut icon" href={favicon} />
            </Helmet>
            <Header />
            {children}
          </GatsbyBodySC>
        );
      }}
    />
  );
};

DefaultLayout.propTypes = {
  children: PropTypes.object,
};

DefaultLayout.defaultProps = {
  children: null,
};

const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`;

export default DefaultLayout;
