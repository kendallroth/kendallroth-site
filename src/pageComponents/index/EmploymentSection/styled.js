import styled from 'styled-components';

/**
 * Employment card grid
 */
const EmploymentGridSC = styled.div`
  display: flex;
  flex-direction: column;
`;

/**
 * Employment section wrapper
 */
const EmploymentSectionSC = styled.section`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 1em;
  padding: 3em 0;
  background-color: #26a69a;

  &:before {
    display: block;
    content: '';
    height: 50%;
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    z-index: -1;
    transform: skewY(-1deg);
    transform-origin: 0 0;
    background: inherit;
    box-shadow: ${({ theme }) => theme.shadows[4]};
  }
`;

/**
 * Divider between hiring prompt and other employment panels
 */
const PanelDividerSC = styled.hr`
  height: 1px;
  margin-right: -8px;
  margin-left: -8px;
  background: rgba(255, 255, 255, 0.8);
`;

export { EmploymentGridSC, EmploymentSectionSC, PanelDividerSC };
