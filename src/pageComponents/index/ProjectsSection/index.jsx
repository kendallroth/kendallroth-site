import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';

// Components
import ProjectCard from '@components/ProjectCard';
import SectionHeader from '@components/SectionHeader';

// Styles
import { ProjectGridSC, ProjectsSectionSC } from './styled';

/**
 * Projects section on Index page
 * @param {object[]} projects - List of projects and images
 */
const ProjectsSection = (props) => {
  const { projects } = props;

  return (
    <ProjectsSectionSC>
      <SectionHeader color="white">What I&rsquo;ve Done</SectionHeader>
      <ProjectGridSC className="grid-container">
        <div className="grid-x grid-margin-x">
          {projects.map((p) => (
            <ProjectCard
              className="cell small-12 medium-6"
              project={p}
              key={p.slug}
            />
          ))}
        </div>
      </ProjectGridSC>
    </ProjectsSectionSC>
  );
};

ProjectsSection.propTypes = {
  projects: PropTypes.arrayOf(
    PropTypes.shape({
      imageSize: PropTypes.shape({
        originalName: PropTypes.string.isRequired,
      }),
      name: PropTypes.string.isRequired,
      slug: PropTypes.string.isRequired,
    })
  ),
};

ProjectsSection.defaultProps = {
  projects: [],
};

export const fragment = graphql`
  # Project markdown files
  fragment ProjectSummaryFileFragment on FileConnection {
    edges {
      node {
        childMarkdownRemark {
          frontmatter {
            image
            slug
            published
            title
            year
          }
        }
      }
    }
  }

  # Related project markdown images
  fragment ProjectSummaryImageFragment on FileConnection {
    edges {
      node {
        childImageSharp {
          sizes {
            # Need to include query from gatsby-images for Image component
            ...GatsbyImageSharpSizes
            originalName
          }
        }
      }
    }
  }
`;

export default ProjectsSection;
