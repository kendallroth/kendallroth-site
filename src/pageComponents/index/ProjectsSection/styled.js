import styled from 'styled-components';

/**
 * Project grid
 */
const ProjectGridSC = styled.div`
  width: 100%;

  /* Fix for Foundation */
  margin-top: 40px !important;
`;

/**
 * Projects section
 */
const ProjectsSectionSC = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3rem 0;
  background-color: ${({ theme }) => theme.palette.blue.main};
  box-shadow: ${({ theme }) => theme.shadows[4]};

  h2 {
    margin: 0;
  }
`;

export { ProjectGridSC, ProjectsSectionSC };
